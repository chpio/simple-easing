# Changelog

## 1.0.1

* repair `elastic_out` function

## 1.0.0

* First release
